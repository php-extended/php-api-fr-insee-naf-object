<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf\Test;

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv6Artisanat;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeNaf2008Lv6ArtisanatTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv6Artisanat
 * @internal
 * @small
 */
class ApiFrInseeNaf2008Lv6ArtisanatTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeNaf2008Lv6Artisanat
	 */
	protected ApiFrInseeNaf2008Lv6Artisanat $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdNaf2008Lv6Artisanat() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNaf2008Lv6Artisanat());
		$this->_object->setIdNaf2008Lv6Artisanat('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNaf2008Lv6Artisanat());
	}
	
	public function testGetIdNaf2008Lv5Subclass() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNaf2008Lv5Subclass());
		$this->_object->setIdNaf2008Lv5Subclass('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNaf2008Lv5Subclass());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeNaf2008Lv6Artisanat('azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
