<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf\Test;

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv1Section;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeNaf2008Lv1SectionTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv1Section
 * @internal
 * @small
 */
class ApiFrInseeNaf2008Lv1SectionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeNaf2008Lv1Section
	 */
	protected ApiFrInseeNaf2008Lv1Section $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdNaf2008Lv1Section() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNaf2008Lv1Section());
		$this->_object->setIdNaf2008Lv1Section('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNaf2008Lv1Section());
	}
	
	public function testGetLibelle40() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle40());
		$this->_object->setLibelle40('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle40());
	}
	
	public function testGetLibelle65() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle65());
		$this->_object->setLibelle65('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle65());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeNaf2008Lv1Section('azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
