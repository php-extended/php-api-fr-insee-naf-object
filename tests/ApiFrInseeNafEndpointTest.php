<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpoint;
use PHPUnit\Framework\TestCase;

/**
 * InseeNafEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeNafEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiFrInseeNafEndpoint
	 */
	protected ApiFrInseeNafEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testNap1973Lv1SectionIterator() : void
	{
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv1Section $nap1973lv1 */
		foreach($this->_object->getNap1973Lv1SectionIterator() as $nap1973lv1)
		{
			$this->assertNotEmpty($nap1973lv1->getIdNap1973Lv1Section());
			$this->assertNotEmpty($nap1973lv1->getLibelle());
		}
	}
	
	public function testNap1973Lv2DivisionIterator() : void
	{
		$nap1973lv1Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv1Section $nap1973lv1 */
		foreach($this->_object->getNap1973Lv1SectionIterator() as $nap1973lv1)
		{
			$nap1973lv1Ids[] = $nap1973lv1->getIdNap1973Lv1Section();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv2Division $nap1973lv2 */
		foreach($this->_object->getNap1973Lv2DivisionIterator() as $nap1973lv2)
		{
			$this->assertNotEmpty($nap1973lv2->getIdNap1973Lv2Division());
			$this->assertNotEmpty($nap1973lv2->getIdNap1973Lv1Section());
			$this->assertContains($nap1973lv2->getIdNap1973Lv1Section(), $nap1973lv1Ids);
			$this->assertNotEmpty($nap1973lv2->getLibelle());
		}
	}
	
	public function testNap1973Lv3GroupIterator() : void
	{
		$nap1973lv2Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv2Division $nap1973lv2 */
		foreach($this->_object->getNap1973Lv2DivisionIterator() as $nap1973lv2)
		{
			$nap1973lv2Ids[] = $nap1973lv2->getIdNap1973Lv2Division();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv3Group $nap1973lv3 */
		foreach($this->_object->getNap1973Lv3GroupIterator() as $nap1973lv3)
		{
			$this->assertNotEmpty($nap1973lv3->getIdNap1973Lv3Group());
			$this->assertNotEmpty($nap1973lv3->getIdNap1973Lv2Division());
			$this->assertContains($nap1973lv3->getIdNap1973Lv2Division(), $nap1973lv2Ids);
			$this->assertNotEmpty($nap1973lv3->getLibelle());
		}
	}
	
	public function testNap1973Lv4ClassIterator() : void
	{
		$nap1973lv3Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv3Group $nap1973lv3 */
		foreach($this->_object->getNap1973lv3GroupIterator() as $nap1973lv3)
		{
			$nap1973lv3Ids[] = $nap1973lv3->getIdNap1973Lv3Group();
		}
		
		$naf1993Lv5Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv4Class $naf1993lv5 */
		foreach($this->_object->getNaf1993Lv5SubclassIterator() as $naf1993lv5)
		{
			$naf1993Lv5Ids[] = $naf1993lv5->getIdNaf1993Lv5Subclass();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv4Class $nap1973lv4 */
		foreach($this->_object->getNap1973Lv4ClassIterator() as $nap1973lv4)
		{
			$this->assertNotEmpty($nap1973lv4->getIdNap1973Lv4Class());
			$this->assertNotEmpty($nap1973lv4->getIdNap1973Lv3Group());
			$this->assertContains($nap1973lv4->getIdNap1973Lv3Group(), $nap1973lv3Ids);
			$this->assertNotEmpty($nap1973lv4->getIdNaf1993Lv5Subclass());
			$this->assertContains($nap1973lv4->getIdNaf1993Lv5Subclass(), $naf1993Lv5Ids);
			$this->assertNotEmpty($nap1973lv4->getLibelle());
		}
	}
	
	public function testNaf1993Lv1SectionIterator() : void
	{
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv1Section $naf1993lv1 */
		foreach($this->_object->getNaf1993Lv1SectionIterator() as $naf1993lv1)
		{
			$this->assertNotEmpty($naf1993lv1->getIdNaf1993Lv1Section());
			$this->assertNotEmpty($naf1993lv1->getLibelle());
		}
	}
	
	public function testNaf1993Lv2DivisionIterator() : void
	{
		$naf1993lv1Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv1Section $naf1993lv1 */
		foreach($this->_object->getNaf1993Lv1SectionIterator() as $naf1993lv1)
		{
			$naf1993lv1Ids[] = $naf1993lv1->getIdNaf1993Lv1Section();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv2Division $naf1993lv2 */
		foreach($this->_object->getNaf1993Lv2DivisionIterator() as $naf1993lv2)
		{
			$this->assertNotEmpty($naf1993lv2->getIdNaf1993Lv2Division());
			$this->assertNotEmpty($naf1993lv2->getIdNaf1993Lv1Section());
			$this->assertContains($naf1993lv2->getIdNaf1993Lv1Section(), $naf1993lv1Ids);
			$this->assertNotEmpty($naf1993lv2->getLibelle());
		}
	}
	
	public function testNaf1993Lv3GroupIterator() : void
	{
		$naf1993lv2Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv2Division $naf1993lv2 */
		foreach($this->_object->getNaf1993Lv2DivisionIterator() as $naf1993lv2)
		{
			$naf1993lv2Ids[] = $naf1993lv2->getIdNaf1993Lv2Division();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv3Group $naf1993lv3 */
		foreach($this->_object->getNaf1993Lv3GroupIterator() as $naf1993lv3)
		{
			$this->assertNotEmpty($naf1993lv3->getIdNaf1993Lv3Group());
			$this->assertNotEmpty($naf1993lv3->getIdNaf1993Lv2Division());
			$this->assertContains($naf1993lv3->getIdNaf1993Lv2Division(), $naf1993lv2Ids);
			$this->assertNotEmpty($naf1993lv3->getLibelle());
		}
	}
	
	public function testNaf1993Lv4ClassIterator() : void
	{
		$naf1993lv3Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv3Group $naf1993lv3 */
		foreach($this->_object->getNaf1993lv3GroupIterator() as $naf1993lv3)
		{
			$naf1993lv3Ids[] = $naf1993lv3->getIdNaf1993Lv3Group();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv4Class $naf1993lv4 */
		foreach($this->_object->getNaf1993Lv4ClassIterator() as $naf1993lv4)
		{
			$this->assertNotEmpty($naf1993lv4->getIdNaf1993Lv4Class());
			$this->assertNotEmpty($naf1993lv4->getIdNaf1993Lv3Group());
			$this->assertContains($naf1993lv4->getIdNaf1993Lv3Group(), $naf1993lv3Ids);
			$this->assertNotEmpty($naf1993lv4->getLibelle());
		}
	}
	
	public function testNaf1993Lv5SublassIterator() : void
	{
		$naf1993lv4Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv4Class $naf1993lv4 */
		foreach($this->_object->getNaf1993lv4ClassIterator() as $naf1993lv4)
		{
			$naf1993lv4Ids[] = $naf1993lv4->getIdNaf1993Lv4Class();
		}
		
		$naf2003lv5Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv5Subclass $naf2003lv5 */
		foreach($this->_object->getNaf2003Lv5SubclassIterator() as $naf2003lv5)
		{
			$naf2003lv5Ids[] = $naf2003lv5->getIdNaf2003Lv5Subclass();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv5Subclass $naf1993lv5 */
		foreach($this->_object->getNaf1993Lv5SubclassIterator() as $naf1993lv5)
		{
			$this->assertNotEmpty($naf1993lv5->getIdNaf1993Lv5Subclass());
			$this->assertNotEmpty($naf1993lv5->getIdNaf1993Lv4Class());
			$this->assertContains($naf1993lv5->getIdNaf1993Lv4Class(), $naf1993lv4Ids);
			$this->assertNotEmpty($naf1993lv5->getIdNaf2003Lv5Subclass());
			$this->assertContains($naf1993lv5->getIdNaf2003Lv5Subclass(), $naf2003lv5Ids);
			$this->assertNotEmpty($naf1993lv5->getLibelle());
		}
	}
	
	public function testNaf2003Lv1SectionIterator() : void
	{
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv1Section $naf2003lv1 */
		foreach($this->_object->getNaf2003Lv1SectionIterator() as $naf2003lv1)
		{
			$this->assertNotEmpty($naf2003lv1->getIdNaf2003Lv1Section());
			$this->assertNotEmpty($naf2003lv1->getLibelle());
		}
	}
	
	public function testNaf2003Lv2DivisionIterator() : void
	{
		$naf2003lv1Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv1Section $naf2003lv1 */
		foreach($this->_object->getNaf2003Lv1SectionIterator() as $naf2003lv1)
		{
			$naf2003lv1Ids[] = $naf2003lv1->getIdNaf2003Lv1Section();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv2Division $naf2003lv2 */
		foreach($this->_object->getNaf2003Lv2DivisionIterator() as $naf2003lv2)
		{
			$this->assertNotEmpty($naf2003lv2->getIdNaf2003Lv2Division());
			$this->assertNotEmpty($naf2003lv2->getIdNaf2003Lv1Section());
			$this->assertContains($naf2003lv2->getIdNaf2003Lv1Section(), $naf2003lv1Ids);
			$this->assertNotEmpty($naf2003lv2->getLibelle());
		}
	}
	
	public function testNaf2003Lv3GroupIterator() : void
	{
		$naf2003lv2Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv2Division $naf2003lv2 */
		foreach($this->_object->getNaf2003Lv2DivisionIterator() as $naf2003lv2)
		{
			$naf2003lv2Ids[] = $naf2003lv2->getIdNaf2003Lv2Division();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv3Group $naf2003lv3 */
		foreach($this->_object->getNaf2003Lv3GroupIterator() as $naf2003lv3)
		{
			$this->assertNotEmpty($naf2003lv3->getIdNaf2003Lv3Group());
			$this->assertNotEmpty($naf2003lv3->getIdNaf2003Lv2Division());
			$this->assertContains($naf2003lv3->getIdNaf2003Lv2Division(), $naf2003lv2Ids);
			$this->assertNotEmpty($naf2003lv3->getLibelle());
		}
	}
	
	public function testNaf2003Lv4ClassIterator() : void
	{
		$naf2003lv3Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv3Group $naf2003lv3 */
		foreach($this->_object->getNaf2003lv3GroupIterator() as $naf2003lv3)
		{
			$naf2003lv3Ids[] = $naf2003lv3->getIdNaf2003Lv3Group();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv4Class $naf2003lv4 */
		foreach($this->_object->getNaf2003Lv4ClassIterator() as $naf2003lv4)
		{
			$this->assertNotEmpty($naf2003lv4->getIdNaf2003Lv4Class());
			$this->assertNotEmpty($naf2003lv4->getIdNaf2003Lv3Group());
			$this->assertContains($naf2003lv4->getIdNaf2003Lv3Group(), $naf2003lv3Ids);
			$this->assertNotEmpty($naf2003lv4->getLibelle());
		}
	}
	
	public function testNaf2003Lv5SublassIterator() : void
	{
		$naf2003lv4Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv4Class $naf2003lv4 */
		foreach($this->_object->getNaf2003lv4ClassIterator() as $naf2003lv4)
		{
			$naf2003lv4Ids[] = $naf2003lv4->getIdNaf2003Lv4Class();
		}
		
		$naf2008lv5Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv5Subclass $naf2008lv5 */
		foreach($this->_object->getNaf2008Lv5SubclassIterator() as $naf2008lv5)
		{
			$naf2008lv5Ids[] = $naf2008lv5->getIdNaf2008Lv5Subclass();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv5Subclass $naf2003lv5 */
		foreach($this->_object->getNaf2003Lv5SubclassIterator() as $naf2003lv5)
		{
			$this->assertNotEmpty($naf2003lv5->getIdNaf2003Lv5Subclass());
			$this->assertNotEmpty($naf2003lv5->getIdNaf2003Lv4Class());
			$this->assertContains($naf2003lv5->getIdNaf2003Lv4Class(), $naf2003lv4Ids);
			$this->assertNotEmpty($naf2003lv5->getIdNaf2008Lv5Subclass());
			$this->assertContains($naf2003lv5->getIdNaf2008Lv5Subclass(), $naf2008lv5Ids);
			$this->assertNotEmpty($naf2003lv5->getLibelle());
		}
	}
	
	public function testNaf2008Lv1SectionIterator() : void
	{
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv1Section $naf2008lv1 */
		foreach($this->_object->getNaf2008Lv1SectionIterator() as $naf2008lv1)
		{
			$this->assertNotEmpty($naf2008lv1->getIdNaf2008Lv1Section());
			$this->assertNotEmpty($naf2008lv1->getLibelle40());
			$this->assertNotEmpty($naf2008lv1->getLibelle65());
			$this->assertNotEmpty($naf2008lv1->getLibelle());
		}
	}
	
	public function testNaf2008Lv2DivisionIterator() : void
	{
		$naf2008lv1Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv1Section $naf2008lv1 */
		foreach($this->_object->getNaf2008Lv1SectionIterator() as $naf2008lv1)
		{
			$naf2008lv1Ids[] = $naf2008lv1->getIdNaf2008Lv1Section();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv2Division $naf2008lv2 */
		foreach($this->_object->getNaf2008Lv2DivisionIterator() as $naf2008lv2)
		{
			$this->assertNotEmpty($naf2008lv2->getIdNaf2008Lv2Division());
			$this->assertNotEmpty($naf2008lv2->getIdNaf2008Lv1Section());
			$this->assertContains($naf2008lv2->getIdNaf2008Lv1Section(), $naf2008lv1Ids);
			$this->assertNotEmpty($naf2008lv2->getLibelle40());
			$this->assertNotEmpty($naf2008lv2->getLibelle65());
			$this->assertNotEmpty($naf2008lv2->getLibelle());
		}
	}
	
	public function testNaf2008Lv3GroupIterator() : void
	{
		$naf2008lv2Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv2Division $naf2008lv2 */
		foreach($this->_object->getNaf2008Lv2DivisionIterator() as $naf2008lv2)
		{
			$naf2008lv2Ids[] = $naf2008lv2->getIdNaf2008Lv2Division();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv3Group $naf2008lv3 */
		foreach($this->_object->getNaf2008Lv3GroupIterator() as $naf2008lv3)
		{
			$this->assertNotEmpty($naf2008lv3->getIdNaf2008Lv3Group());
			$this->assertNotEmpty($naf2008lv3->getIdNaf2008Lv2Division());
			$this->assertContains($naf2008lv3->getIdNaf2008Lv2Division(), $naf2008lv2Ids);
			$this->assertNotEmpty($naf2008lv3->getLibelle40());
			$this->assertNotEmpty($naf2008lv3->getLibelle65());
			$this->assertNotEmpty($naf2008lv3->getLibelle());
		}
	}
	
	public function testNaf2008Lv4ClassIterator() : void
	{
		$naf2008lv3Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv3Group $naf2008lv3 */
		foreach($this->_object->getNaf2008lv3GroupIterator() as $naf2008lv3)
		{
			$naf2008lv3Ids[] = $naf2008lv3->getIdNaf2008Lv3Group();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv4Class $naf2008lv4 */
		foreach($this->_object->getNaf2008Lv4ClassIterator() as $naf2008lv4)
		{
			$this->assertNotEmpty($naf2008lv4->getIdNaf2008Lv4Class());
			$this->assertNotEmpty($naf2008lv4->getIdNaf2008Lv3Group());
			$this->assertContains($naf2008lv4->getIdNaf2008Lv3Group(), $naf2008lv3Ids);
			$this->assertNotEmpty($naf2008lv4->getLibelle40());
			$this->assertNotEmpty($naf2008lv4->getLibelle65());
			$this->assertNotEmpty($naf2008lv4->getLibelle());
		}
	}
	
	public function testNaf2008Lv5SublassIterator() : void
	{
		$naf2008lv4Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv4Class $naf2008lv4 */
		foreach($this->_object->getNaf2008lv4ClassIterator() as $naf2008lv4)
		{
			$naf2008lv4Ids[] = $naf2008lv4->getIdNaf2008Lv4Class();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv5Subclass $naf2008lv5 */
		foreach($this->_object->getNaf2008Lv5SubclassIterator() as $naf2008lv5)
		{
			$this->assertNotEmpty($naf2008lv5->getIdNaf2008Lv5Subclass());
			$this->assertNotEmpty($naf2008lv5->getIdNaf2008Lv4Class());
			$this->assertContains($naf2008lv5->getIdNaf2008Lv4Class(), $naf2008lv4Ids);
			$this->assertNotEmpty($naf2008lv5->getLibelle40());
			$this->assertNotEmpty($naf2008lv5->getLibelle65());
			$this->assertNotEmpty($naf2008lv5->getLibelle());
		}
	}
	
	public function testNaf2008Lv6ArtisanatIterator() : void
	{
		$naf2008lv5Ids = [];
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv5Subclass $naf2008lv5 */
		foreach($this->_object->getNaf2008Lv5SubclassIterator() as $naf2008lv5)
		{
			$naf2008lv5Ids[] = $naf2008lv5->getIdNaf2008Lv5Subclass();
		}
		
		/** @var PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv6Artisanat $naf2008lv6 */
		foreach($this->_object->getNaf2008Lv6ArtisanatIterator() as $naf2008lv6)
		{
			$this->assertNotEmpty($naf2008lv6->getIdNaf2008Lv6Artisanat());
			$this->assertNotEmpty($naf2008lv6->getIdNaf2008Lv5Subclass());
			$this->assertContains($naf2008lv6->getIdNaf2008Lv5Subclass(), $naf2008lv5Ids);
			$this->assertNotEmpty($naf2008lv6->getLibelle());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeNafEndpoint();
	}
	
}
