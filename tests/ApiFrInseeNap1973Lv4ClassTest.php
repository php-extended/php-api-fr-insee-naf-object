<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf\Test;

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv4Class;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeNap1973Lv4ClassTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv4Class
 * @internal
 * @small
 */
class ApiFrInseeNap1973Lv4ClassTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeNap1973Lv4Class
	 */
	protected ApiFrInseeNap1973Lv4Class $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdNap1973Lv4Class() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNap1973Lv4Class());
		$this->_object->setIdNap1973Lv4Class('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNap1973Lv4Class());
	}
	
	public function testGetIdNap1973Lv3Group() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNap1973Lv3Group());
		$this->_object->setIdNap1973Lv3Group('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNap1973Lv3Group());
	}
	
	public function testGetIdNaf1993Lv5Subclass() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNaf1993Lv5Subclass());
		$this->_object->setIdNaf1993Lv5Subclass('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNaf1993Lv5Subclass());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeNap1973Lv4Class('azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
