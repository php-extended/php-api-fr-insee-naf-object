<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf\Test;

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv1Section;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeNaf1993Lv1SectionTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv1Section
 * @internal
 * @small
 */
class ApiFrInseeNaf1993Lv1SectionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeNaf1993Lv1Section
	 */
	protected ApiFrInseeNaf1993Lv1Section $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdNaf1993Lv1Section() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdNaf1993Lv1Section());
		$this->_object->setIdNaf1993Lv1Section('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdNaf1993Lv1Section());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$this->_object->setLibelle('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeNaf1993Lv1Section('azertyuiop', 'azertyuiop');
	}
	
}
