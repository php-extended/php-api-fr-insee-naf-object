<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf1993Lv3Group class file.
 * 
 * This is a simple implementation of the ApiFrInseeNaf1993Lv3GroupInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf1993Lv3Group implements ApiFrInseeNaf1993Lv3GroupInterface
{
	
	/**
	 * The id of this group.
	 * 
	 * @var string
	 */
	protected string $_idNaf1993Lv3Group;
	
	/**
	 * The id of the related division.
	 * 
	 * @var string
	 */
	protected string $_idNaf1993Lv2Division;
	
	/**
	 * The libelle of this group.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf1993Lv3Group with private members.
	 * 
	 * @param string $idNaf1993Lv3Group
	 * @param string $idNaf1993Lv2Division
	 * @param string $libelle
	 */
	public function __construct(string $idNaf1993Lv3Group, string $idNaf1993Lv2Division, string $libelle)
	{
		$this->setIdNaf1993Lv3Group($idNaf1993Lv3Group);
		$this->setIdNaf1993Lv2Division($idNaf1993Lv2Division);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this group.
	 * 
	 * @param string $idNaf1993Lv3Group
	 * @return ApiFrInseeNaf1993Lv3GroupInterface
	 */
	public function setIdNaf1993Lv3Group(string $idNaf1993Lv3Group) : ApiFrInseeNaf1993Lv3GroupInterface
	{
		$this->_idNaf1993Lv3Group = $idNaf1993Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of this group.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv3Group() : string
	{
		return $this->_idNaf1993Lv3Group;
	}
	
	/**
	 * Sets the id of the related division.
	 * 
	 * @param string $idNaf1993Lv2Division
	 * @return ApiFrInseeNaf1993Lv3GroupInterface
	 */
	public function setIdNaf1993Lv2Division(string $idNaf1993Lv2Division) : ApiFrInseeNaf1993Lv3GroupInterface
	{
		$this->_idNaf1993Lv2Division = $idNaf1993Lv2Division;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related division.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv2Division() : string
	{
		return $this->_idNaf1993Lv2Division;
	}
	
	/**
	 * Sets the libelle of this group.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf1993Lv3GroupInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf1993Lv3GroupInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this group.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
