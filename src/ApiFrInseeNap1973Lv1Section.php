<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNap1973Lv1Section class file.
 * 
 * This is a simple implementation of the ApiFrInseeNap1973Lv1SectionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNap1973Lv1Section implements ApiFrInseeNap1973Lv1SectionInterface
{
	
	/**
	 * The id of this section.
	 * 
	 * @var string
	 */
	protected string $_idNap1973Lv1Section;
	
	/**
	 * The libelle of this section.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNap1973Lv1Section with private members.
	 * 
	 * @param string $idNap1973Lv1Section
	 * @param string $libelle
	 */
	public function __construct(string $idNap1973Lv1Section, string $libelle)
	{
		$this->setIdNap1973Lv1Section($idNap1973Lv1Section);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this section.
	 * 
	 * @param string $idNap1973Lv1Section
	 * @return ApiFrInseeNap1973Lv1SectionInterface
	 */
	public function setIdNap1973Lv1Section(string $idNap1973Lv1Section) : ApiFrInseeNap1973Lv1SectionInterface
	{
		$this->_idNap1973Lv1Section = $idNap1973Lv1Section;
		
		return $this;
	}
	
	/**
	 * Gets the id of this section.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv1Section() : string
	{
		return $this->_idNap1973Lv1Section;
	}
	
	/**
	 * Sets the libelle of this section.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNap1973Lv1SectionInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNap1973Lv1SectionInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this section.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
