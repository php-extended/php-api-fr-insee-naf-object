<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNap1973Lv4Class class file.
 * 
 * This is a simple implementation of the ApiFrInseeNap1973Lv4ClassInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNap1973Lv4Class implements ApiFrInseeNap1973Lv4ClassInterface
{
	
	/**
	 * The id of this class.
	 * 
	 * @var string
	 */
	protected string $_idNap1973Lv4Class;
	
	/**
	 * The id of the related group.
	 * 
	 * @var string
	 */
	protected string $_idNap1973Lv3Group;
	
	/**
	 * The id of the related next subclass.
	 * 
	 * @var string
	 */
	protected string $_idNaf1993Lv5Subclass;
	
	/**
	 * The libelle of this class.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNap1973Lv4Class with private members.
	 * 
	 * @param string $idNap1973Lv4Class
	 * @param string $idNap1973Lv3Group
	 * @param string $idNaf1993Lv5Subclass
	 * @param string $libelle
	 */
	public function __construct(string $idNap1973Lv4Class, string $idNap1973Lv3Group, string $idNaf1993Lv5Subclass, string $libelle)
	{
		$this->setIdNap1973Lv4Class($idNap1973Lv4Class);
		$this->setIdNap1973Lv3Group($idNap1973Lv3Group);
		$this->setIdNaf1993Lv5Subclass($idNaf1993Lv5Subclass);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this class.
	 * 
	 * @param string $idNap1973Lv4Class
	 * @return ApiFrInseeNap1973Lv4ClassInterface
	 */
	public function setIdNap1973Lv4Class(string $idNap1973Lv4Class) : ApiFrInseeNap1973Lv4ClassInterface
	{
		$this->_idNap1973Lv4Class = $idNap1973Lv4Class;
		
		return $this;
	}
	
	/**
	 * Gets the id of this class.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv4Class() : string
	{
		return $this->_idNap1973Lv4Class;
	}
	
	/**
	 * Sets the id of the related group.
	 * 
	 * @param string $idNap1973Lv3Group
	 * @return ApiFrInseeNap1973Lv4ClassInterface
	 */
	public function setIdNap1973Lv3Group(string $idNap1973Lv3Group) : ApiFrInseeNap1973Lv4ClassInterface
	{
		$this->_idNap1973Lv3Group = $idNap1973Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related group.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv3Group() : string
	{
		return $this->_idNap1973Lv3Group;
	}
	
	/**
	 * Sets the id of the related next subclass.
	 * 
	 * @param string $idNaf1993Lv5Subclass
	 * @return ApiFrInseeNap1973Lv4ClassInterface
	 */
	public function setIdNaf1993Lv5Subclass(string $idNaf1993Lv5Subclass) : ApiFrInseeNap1973Lv4ClassInterface
	{
		$this->_idNaf1993Lv5Subclass = $idNaf1993Lv5Subclass;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related next subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv5Subclass() : string
	{
		return $this->_idNaf1993Lv5Subclass;
	}
	
	/**
	 * Sets the libelle of this class.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNap1973Lv4ClassInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNap1973Lv4ClassInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this class.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
