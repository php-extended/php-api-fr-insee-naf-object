<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNap1973Lv3Group class file.
 * 
 * This is a simple implementation of the ApiFrInseeNap1973Lv3GroupInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNap1973Lv3Group implements ApiFrInseeNap1973Lv3GroupInterface
{
	
	/**
	 * The id of this group.
	 * 
	 * @var string
	 */
	protected string $_idNap1973Lv3Group;
	
	/**
	 * The id of the related division.
	 * 
	 * @var string
	 */
	protected string $_idNap1973Lv2Division;
	
	/**
	 * The libelle of this group.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNap1973Lv3Group with private members.
	 * 
	 * @param string $idNap1973Lv3Group
	 * @param string $idNap1973Lv2Division
	 * @param string $libelle
	 */
	public function __construct(string $idNap1973Lv3Group, string $idNap1973Lv2Division, string $libelle)
	{
		$this->setIdNap1973Lv3Group($idNap1973Lv3Group);
		$this->setIdNap1973Lv2Division($idNap1973Lv2Division);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this group.
	 * 
	 * @param string $idNap1973Lv3Group
	 * @return ApiFrInseeNap1973Lv3GroupInterface
	 */
	public function setIdNap1973Lv3Group(string $idNap1973Lv3Group) : ApiFrInseeNap1973Lv3GroupInterface
	{
		$this->_idNap1973Lv3Group = $idNap1973Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of this group.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv3Group() : string
	{
		return $this->_idNap1973Lv3Group;
	}
	
	/**
	 * Sets the id of the related division.
	 * 
	 * @param string $idNap1973Lv2Division
	 * @return ApiFrInseeNap1973Lv3GroupInterface
	 */
	public function setIdNap1973Lv2Division(string $idNap1973Lv2Division) : ApiFrInseeNap1973Lv3GroupInterface
	{
		$this->_idNap1973Lv2Division = $idNap1973Lv2Division;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related division.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv2Division() : string
	{
		return $this->_idNap1973Lv2Division;
	}
	
	/**
	 * Sets the libelle of this group.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNap1973Lv3GroupInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNap1973Lv3GroupInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this group.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
