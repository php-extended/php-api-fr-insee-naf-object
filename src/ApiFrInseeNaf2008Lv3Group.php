<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2008Lv3Group class file.
 * 
 * This is a simple implementation of the ApiFrInseeNaf2008Lv3GroupInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2008Lv3Group implements ApiFrInseeNaf2008Lv3GroupInterface
{
	
	/**
	 * The id of this group.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv3Group;
	
	/**
	 * The id of the related division.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv2Division;
	
	/**
	 * The libelle, reduced to 40 chars max.
	 * 
	 * @var string
	 */
	protected string $_libelle40;
	
	/**
	 * The libelle, reduced to 65 chars max.
	 * 
	 * @var string
	 */
	protected string $_libelle65;
	
	/**
	 * The libelle of this group.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2008Lv3Group with private members.
	 * 
	 * @param string $idNaf2008Lv3Group
	 * @param string $idNaf2008Lv2Division
	 * @param string $libelle40
	 * @param string $libelle65
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2008Lv3Group, string $idNaf2008Lv2Division, string $libelle40, string $libelle65, string $libelle)
	{
		$this->setIdNaf2008Lv3Group($idNaf2008Lv3Group);
		$this->setIdNaf2008Lv2Division($idNaf2008Lv2Division);
		$this->setLibelle40($libelle40);
		$this->setLibelle65($libelle65);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this group.
	 * 
	 * @param string $idNaf2008Lv3Group
	 * @return ApiFrInseeNaf2008Lv3GroupInterface
	 */
	public function setIdNaf2008Lv3Group(string $idNaf2008Lv3Group) : ApiFrInseeNaf2008Lv3GroupInterface
	{
		$this->_idNaf2008Lv3Group = $idNaf2008Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of this group.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv3Group() : string
	{
		return $this->_idNaf2008Lv3Group;
	}
	
	/**
	 * Sets the id of the related division.
	 * 
	 * @param string $idNaf2008Lv2Division
	 * @return ApiFrInseeNaf2008Lv3GroupInterface
	 */
	public function setIdNaf2008Lv2Division(string $idNaf2008Lv2Division) : ApiFrInseeNaf2008Lv3GroupInterface
	{
		$this->_idNaf2008Lv2Division = $idNaf2008Lv2Division;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related division.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv2Division() : string
	{
		return $this->_idNaf2008Lv2Division;
	}
	
	/**
	 * Sets the libelle, reduced to 40 chars max.
	 * 
	 * @param string $libelle40
	 * @return ApiFrInseeNaf2008Lv3GroupInterface
	 */
	public function setLibelle40(string $libelle40) : ApiFrInseeNaf2008Lv3GroupInterface
	{
		$this->_libelle40 = $libelle40;
		
		return $this;
	}
	
	/**
	 * Gets the libelle, reduced to 40 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle40() : string
	{
		return $this->_libelle40;
	}
	
	/**
	 * Sets the libelle, reduced to 65 chars max.
	 * 
	 * @param string $libelle65
	 * @return ApiFrInseeNaf2008Lv3GroupInterface
	 */
	public function setLibelle65(string $libelle65) : ApiFrInseeNaf2008Lv3GroupInterface
	{
		$this->_libelle65 = $libelle65;
		
		return $this;
	}
	
	/**
	 * Gets the libelle, reduced to 65 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle65() : string
	{
		return $this->_libelle65;
	}
	
	/**
	 * Sets the libelle of this group.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2008Lv3GroupInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2008Lv3GroupInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this group.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
