<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2008Lv5Subclass class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeNaf2008Lv5SubclassInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2008Lv5Subclass implements ApiFrInseeNaf2008Lv5SubclassInterface
{
	
	/**
	 * The id of this subclass.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv5Subclass;
	
	/**
	 * The id of the related class.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv4Class;
	
	/**
	 * The libelle, reduced to 40 chars max.
	 * 
	 * @var string
	 */
	protected string $_libelle40;
	
	/**
	 * The libelle, reduced to 65 chars max.
	 * 
	 * @var string
	 */
	protected string $_libelle65;
	
	/**
	 * The libelle of this subclass.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2008Lv5Subclass with private members.
	 * 
	 * @param string $idNaf2008Lv5Subclass
	 * @param string $idNaf2008Lv4Class
	 * @param string $libelle40
	 * @param string $libelle65
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2008Lv5Subclass, string $idNaf2008Lv4Class, string $libelle40, string $libelle65, string $libelle)
	{
		$this->setIdNaf2008Lv5Subclass($idNaf2008Lv5Subclass);
		$this->setIdNaf2008Lv4Class($idNaf2008Lv4Class);
		$this->setLibelle40($libelle40);
		$this->setLibelle65($libelle65);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this subclass.
	 * 
	 * @param string $idNaf2008Lv5Subclass
	 * @return ApiFrInseeNaf2008Lv5SubclassInterface
	 */
	public function setIdNaf2008Lv5Subclass(string $idNaf2008Lv5Subclass) : ApiFrInseeNaf2008Lv5SubclassInterface
	{
		$this->_idNaf2008Lv5Subclass = $idNaf2008Lv5Subclass;
		
		return $this;
	}
	
	/**
	 * Gets the id of this subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv5Subclass() : string
	{
		return $this->_idNaf2008Lv5Subclass;
	}
	
	/**
	 * Sets the id of the related class.
	 * 
	 * @param string $idNaf2008Lv4Class
	 * @return ApiFrInseeNaf2008Lv5SubclassInterface
	 */
	public function setIdNaf2008Lv4Class(string $idNaf2008Lv4Class) : ApiFrInseeNaf2008Lv5SubclassInterface
	{
		$this->_idNaf2008Lv4Class = $idNaf2008Lv4Class;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related class.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv4Class() : string
	{
		return $this->_idNaf2008Lv4Class;
	}
	
	/**
	 * Sets the libelle, reduced to 40 chars max.
	 * 
	 * @param string $libelle40
	 * @return ApiFrInseeNaf2008Lv5SubclassInterface
	 */
	public function setLibelle40(string $libelle40) : ApiFrInseeNaf2008Lv5SubclassInterface
	{
		$this->_libelle40 = $libelle40;
		
		return $this;
	}
	
	/**
	 * Gets the libelle, reduced to 40 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle40() : string
	{
		return $this->_libelle40;
	}
	
	/**
	 * Sets the libelle, reduced to 65 chars max.
	 * 
	 * @param string $libelle65
	 * @return ApiFrInseeNaf2008Lv5SubclassInterface
	 */
	public function setLibelle65(string $libelle65) : ApiFrInseeNaf2008Lv5SubclassInterface
	{
		$this->_libelle65 = $libelle65;
		
		return $this;
	}
	
	/**
	 * Gets the libelle, reduced to 65 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle65() : string
	{
		return $this->_libelle65;
	}
	
	/**
	 * Sets the libelle of this subclass.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2008Lv5SubclassInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2008Lv5SubclassInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this subclass.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
