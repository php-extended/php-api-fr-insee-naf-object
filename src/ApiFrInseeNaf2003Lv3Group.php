<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2003Lv3Group class file.
 * 
 * This is a simple implementation of the ApiFrInseeNaf2003Lv3GroupInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2003Lv3Group implements ApiFrInseeNaf2003Lv3GroupInterface
{
	
	/**
	 * The id of this group.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv3Group;
	
	/**
	 * The id of the related division.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv2Division;
	
	/**
	 * The libelle of this group.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2003Lv3Group with private members.
	 * 
	 * @param string $idNaf2003Lv3Group
	 * @param string $idNaf2003Lv2Division
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2003Lv3Group, string $idNaf2003Lv2Division, string $libelle)
	{
		$this->setIdNaf2003Lv3Group($idNaf2003Lv3Group);
		$this->setIdNaf2003Lv2Division($idNaf2003Lv2Division);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this group.
	 * 
	 * @param string $idNaf2003Lv3Group
	 * @return ApiFrInseeNaf2003Lv3GroupInterface
	 */
	public function setIdNaf2003Lv3Group(string $idNaf2003Lv3Group) : ApiFrInseeNaf2003Lv3GroupInterface
	{
		$this->_idNaf2003Lv3Group = $idNaf2003Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of this group.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv3Group() : string
	{
		return $this->_idNaf2003Lv3Group;
	}
	
	/**
	 * Sets the id of the related division.
	 * 
	 * @param string $idNaf2003Lv2Division
	 * @return ApiFrInseeNaf2003Lv3GroupInterface
	 */
	public function setIdNaf2003Lv2Division(string $idNaf2003Lv2Division) : ApiFrInseeNaf2003Lv3GroupInterface
	{
		$this->_idNaf2003Lv2Division = $idNaf2003Lv2Division;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related division.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv2Division() : string
	{
		return $this->_idNaf2003Lv2Division;
	}
	
	/**
	 * Sets the libelle of this group.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2003Lv3GroupInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2003Lv3GroupInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this group.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
