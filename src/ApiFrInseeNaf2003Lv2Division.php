<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2003Lv2Division class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeNaf2003Lv2DivisionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2003Lv2Division implements ApiFrInseeNaf2003Lv2DivisionInterface
{
	
	/**
	 * The id of this division.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv2Division;
	
	/**
	 * The id of the related section.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv1Section;
	
	/**
	 * The libelle of this division.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2003Lv2Division with private members.
	 * 
	 * @param string $idNaf2003Lv2Division
	 * @param string $idNaf2003Lv1Section
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2003Lv2Division, string $idNaf2003Lv1Section, string $libelle)
	{
		$this->setIdNaf2003Lv2Division($idNaf2003Lv2Division);
		$this->setIdNaf2003Lv1Section($idNaf2003Lv1Section);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this division.
	 * 
	 * @param string $idNaf2003Lv2Division
	 * @return ApiFrInseeNaf2003Lv2DivisionInterface
	 */
	public function setIdNaf2003Lv2Division(string $idNaf2003Lv2Division) : ApiFrInseeNaf2003Lv2DivisionInterface
	{
		$this->_idNaf2003Lv2Division = $idNaf2003Lv2Division;
		
		return $this;
	}
	
	/**
	 * Gets the id of this division.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv2Division() : string
	{
		return $this->_idNaf2003Lv2Division;
	}
	
	/**
	 * Sets the id of the related section.
	 * 
	 * @param string $idNaf2003Lv1Section
	 * @return ApiFrInseeNaf2003Lv2DivisionInterface
	 */
	public function setIdNaf2003Lv1Section(string $idNaf2003Lv1Section) : ApiFrInseeNaf2003Lv2DivisionInterface
	{
		$this->_idNaf2003Lv1Section = $idNaf2003Lv1Section;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related section.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv1Section() : string
	{
		return $this->_idNaf2003Lv1Section;
	}
	
	/**
	 * Sets the libelle of this division.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2003Lv2DivisionInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2003Lv2DivisionInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this division.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
