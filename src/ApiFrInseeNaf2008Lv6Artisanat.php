<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2008Lv6Artisanat class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeNaf2008Lv6ArtisanatInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2008Lv6Artisanat implements ApiFrInseeNaf2008Lv6ArtisanatInterface
{
	
	/**
	 * The id of this artisanat.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv6Artisanat;
	
	/**
	 * The id of the related subclass.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv5Subclass;
	
	/**
	 * The libelle of this artisanat.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2008Lv6Artisanat with private members.
	 * 
	 * @param string $idNaf2008Lv6Artisanat
	 * @param string $idNaf2008Lv5Subclass
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2008Lv6Artisanat, string $idNaf2008Lv5Subclass, string $libelle)
	{
		$this->setIdNaf2008Lv6Artisanat($idNaf2008Lv6Artisanat);
		$this->setIdNaf2008Lv5Subclass($idNaf2008Lv5Subclass);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this artisanat.
	 * 
	 * @param string $idNaf2008Lv6Artisanat
	 * @return ApiFrInseeNaf2008Lv6ArtisanatInterface
	 */
	public function setIdNaf2008Lv6Artisanat(string $idNaf2008Lv6Artisanat) : ApiFrInseeNaf2008Lv6ArtisanatInterface
	{
		$this->_idNaf2008Lv6Artisanat = $idNaf2008Lv6Artisanat;
		
		return $this;
	}
	
	/**
	 * Gets the id of this artisanat.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv6Artisanat() : string
	{
		return $this->_idNaf2008Lv6Artisanat;
	}
	
	/**
	 * Sets the id of the related subclass.
	 * 
	 * @param string $idNaf2008Lv5Subclass
	 * @return ApiFrInseeNaf2008Lv6ArtisanatInterface
	 */
	public function setIdNaf2008Lv5Subclass(string $idNaf2008Lv5Subclass) : ApiFrInseeNaf2008Lv6ArtisanatInterface
	{
		$this->_idNaf2008Lv5Subclass = $idNaf2008Lv5Subclass;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv5Subclass() : string
	{
		return $this->_idNaf2008Lv5Subclass;
	}
	
	/**
	 * Sets the libelle of this artisanat.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2008Lv6ArtisanatInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2008Lv6ArtisanatInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this artisanat.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
