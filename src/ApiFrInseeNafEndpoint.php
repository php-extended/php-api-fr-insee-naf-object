<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Iterator;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;

/**
 * InseeNafEndpoint class file.
 * 
 * This class is a simple implementation of the InseeNafEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrInseeNafEndpoint implements ApiFrInseeNafEndpointInterface
{
	
	/**
	 * The reifier.
	 * 
	 * @var ?ReifierInterface
	 */
	protected ?ReifierInterface $_reifier = null;
	
	/**
	 * Builds a new endpoint with the given reifier.
	 * 
	 * @param ReifierInterface $reifier
	 */
	public function __construct(?ReifierInterface $reifier = null)
	{
		$this->_reifier = $reifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNap1973Lv1SectionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNap1973Lv1SectionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/nap1973lv1.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNap1973Lv1Section::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNap1973Lv2DivisionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNap1973Lv2DivisionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/nap1973lv2.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNap1973Lv2Division::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNap1973Lv3GroupIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNap1973Lv3GroupIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/nap1973lv3.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNap1973Lv3Group::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNap1973Lv4ClassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNap1973Lv4ClassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/nap1973lv4.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNap1973Lv4Class::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf1993Lv1SectionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf1993Lv1SectionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf1993lv1.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf1993Lv1Section::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf1993Lv2DivisionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf1993Lv2DivisionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf1993lv2.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf1993Lv2Division::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf1993Lv3GroupIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf1993Lv3GroupIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf1993lv3.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf1993Lv3Group::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf1993Lv4ClassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf1993Lv4ClassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf1993lv4.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf1993Lv4Class::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf1993Lv5SubclassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf1993Lv5SubclassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf1993lv5.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf1993Lv5Subclass::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2003Lv1SectionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2003Lv1SectionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2003lv1.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2003Lv1Section::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2003Lv2DivisionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2003Lv2DivisionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2003lv2.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2003Lv2Division::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2003Lv3GroupIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2003Lv3GroupIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2003lv3.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2003Lv3Group::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2003Lv4ClassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2003Lv4ClassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2003lv4.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2003Lv4Class::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2003Lv5SubclassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2003Lv5SubclassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2003lv5.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2003Lv5Subclass::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv1SectionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv1SectionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv1.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv1Section::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv2DivisionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv2DivisionIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv2.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv2Division::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv3GroupIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv3GroupIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv3.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv3Group::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv4ClassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv4ClassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv4.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv4Class::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv5SubclassIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv5SubclassIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv5.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv5Subclass::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpointInterface::getNaf2008Lv6ArtisanatIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNaf2008Lv6ArtisanatIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/naf2008lv6.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeNaf2008Lv6Artisanat::class, $iterator);
	}
	
	/**
	 * Gets the reifier.
	 * 
	 * @return ReifierInterface
	 */
	protected function getReifier() : ReifierInterface
	{
		if(null === $this->_reifier)
		{
			$this->_reifier = new Reifier();
		}
		
		return $this->_reifier;
	}
	
}
