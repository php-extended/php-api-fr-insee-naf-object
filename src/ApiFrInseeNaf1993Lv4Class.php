<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf1993Lv4Class class file.
 * 
 * This is a simple implementation of the ApiFrInseeNaf1993Lv4ClassInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf1993Lv4Class implements ApiFrInseeNaf1993Lv4ClassInterface
{
	
	/**
	 * The id of this class.
	 * 
	 * @var string
	 */
	protected string $_idNaf1993Lv4Class;
	
	/**
	 * The id of the related group.
	 * 
	 * @var string
	 */
	protected string $_idNaf1993Lv3Group;
	
	/**
	 * The libelle of this class.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf1993Lv4Class with private members.
	 * 
	 * @param string $idNaf1993Lv4Class
	 * @param string $idNaf1993Lv3Group
	 * @param string $libelle
	 */
	public function __construct(string $idNaf1993Lv4Class, string $idNaf1993Lv3Group, string $libelle)
	{
		$this->setIdNaf1993Lv4Class($idNaf1993Lv4Class);
		$this->setIdNaf1993Lv3Group($idNaf1993Lv3Group);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this class.
	 * 
	 * @param string $idNaf1993Lv4Class
	 * @return ApiFrInseeNaf1993Lv4ClassInterface
	 */
	public function setIdNaf1993Lv4Class(string $idNaf1993Lv4Class) : ApiFrInseeNaf1993Lv4ClassInterface
	{
		$this->_idNaf1993Lv4Class = $idNaf1993Lv4Class;
		
		return $this;
	}
	
	/**
	 * Gets the id of this class.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv4Class() : string
	{
		return $this->_idNaf1993Lv4Class;
	}
	
	/**
	 * Sets the id of the related group.
	 * 
	 * @param string $idNaf1993Lv3Group
	 * @return ApiFrInseeNaf1993Lv4ClassInterface
	 */
	public function setIdNaf1993Lv3Group(string $idNaf1993Lv3Group) : ApiFrInseeNaf1993Lv4ClassInterface
	{
		$this->_idNaf1993Lv3Group = $idNaf1993Lv3Group;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related group.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv3Group() : string
	{
		return $this->_idNaf1993Lv3Group;
	}
	
	/**
	 * Sets the libelle of this class.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf1993Lv4ClassInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf1993Lv4ClassInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this class.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
