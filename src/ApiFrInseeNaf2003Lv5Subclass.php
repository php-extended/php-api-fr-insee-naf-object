<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

/**
 * ApiFrInseeNaf2003Lv5Subclass class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeNaf2003Lv5SubclassInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeNaf2003Lv5Subclass implements ApiFrInseeNaf2003Lv5SubclassInterface
{
	
	/**
	 * The id of this subclass.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv5Subclass;
	
	/**
	 * The id of the related class.
	 * 
	 * @var string
	 */
	protected string $_idNaf2003Lv4Class;
	
	/**
	 * The id of the related next subclass.
	 * 
	 * @var string
	 */
	protected string $_idNaf2008Lv5Subclass;
	
	/**
	 * The libelle of this subclass.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeNaf2003Lv5Subclass with private members.
	 * 
	 * @param string $idNaf2003Lv5Subclass
	 * @param string $idNaf2003Lv4Class
	 * @param string $idNaf2008Lv5Subclass
	 * @param string $libelle
	 */
	public function __construct(string $idNaf2003Lv5Subclass, string $idNaf2003Lv4Class, string $idNaf2008Lv5Subclass, string $libelle)
	{
		$this->setIdNaf2003Lv5Subclass($idNaf2003Lv5Subclass);
		$this->setIdNaf2003Lv4Class($idNaf2003Lv4Class);
		$this->setIdNaf2008Lv5Subclass($idNaf2008Lv5Subclass);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this subclass.
	 * 
	 * @param string $idNaf2003Lv5Subclass
	 * @return ApiFrInseeNaf2003Lv5SubclassInterface
	 */
	public function setIdNaf2003Lv5Subclass(string $idNaf2003Lv5Subclass) : ApiFrInseeNaf2003Lv5SubclassInterface
	{
		$this->_idNaf2003Lv5Subclass = $idNaf2003Lv5Subclass;
		
		return $this;
	}
	
	/**
	 * Gets the id of this subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv5Subclass() : string
	{
		return $this->_idNaf2003Lv5Subclass;
	}
	
	/**
	 * Sets the id of the related class.
	 * 
	 * @param string $idNaf2003Lv4Class
	 * @return ApiFrInseeNaf2003Lv5SubclassInterface
	 */
	public function setIdNaf2003Lv4Class(string $idNaf2003Lv4Class) : ApiFrInseeNaf2003Lv5SubclassInterface
	{
		$this->_idNaf2003Lv4Class = $idNaf2003Lv4Class;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related class.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv4Class() : string
	{
		return $this->_idNaf2003Lv4Class;
	}
	
	/**
	 * Sets the id of the related next subclass.
	 * 
	 * @param string $idNaf2008Lv5Subclass
	 * @return ApiFrInseeNaf2003Lv5SubclassInterface
	 */
	public function setIdNaf2008Lv5Subclass(string $idNaf2008Lv5Subclass) : ApiFrInseeNaf2003Lv5SubclassInterface
	{
		$this->_idNaf2008Lv5Subclass = $idNaf2008Lv5Subclass;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related next subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv5Subclass() : string
	{
		return $this->_idNaf2008Lv5Subclass;
	}
	
	/**
	 * Sets the libelle of this subclass.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeNaf2003Lv5SubclassInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeNaf2003Lv5SubclassInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this subclass.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
