# php-extended/php-api-fr-insee-naf-object

A library that implements the php-extended/php-api-fr-insee-naf-interface library.

A library that makes insee Nomenclature des Activités Françaises (NAF) available
as objects to populate a database. The NAF is available at the url :
[https://www.insee.fr/fr/information/2120875](https://www.insee.fr/fr/information/2120875).

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-naf-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-naf-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-naf-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Insee\InseeNafEndpoint;

$endpoint   = new InseeNafEndpoint();

// for the latest norm (2008) :
$sections   = $endpoint->getNaf2008Lv1SectionIterator();   // returns iterator of InseeNaf2008Lv1Section 
$divisions  = $endpoint->getNaf2008Lv2DivisionIterator();  // returns iterator of InseeNaf2008Lv2Division
$groups     = $endpoint->getNaf2008Lv3GroupIterator();     // returns iterator of InseeNaf2008Lv3Group
$classes    = $endpoint->getNaf2008Lv4ClassIterator();     // returns iterator of InseeNaf2008Lv4Class
$subclasses = $endpoint->getNaf2008Lv5SubclassIterator();  // returns iterator of InseeNaf2008Lv5Subclass
$artisanats = $endpoint->getNaf2008Lv6ArtisanatIterator(); // returns iterator of InseeNaf2008Lv6Artisanat

// for the previous norm (2003) : 
$sections   = $endpoint->getNaf2003Lv1SectionIterator();  // returns iterator of InseeNaf2003Lv1Section
$divisions  = $endpoint->getNaf2003Lv2DivisionIterator(); // returns iterator of InseeNaf2003Lv2Division
$groups     = $endpoint->getNaf2003Lv3GroupIterator();    // returns iterator of InseeNaf2003Lv3Group
$classes    = $endpoint->getNaf2003Lv4ClassIterator();    // returns iterator of InseeNaf2003Lv4Class
$subclasses = $endpoint->getNaf2003Lv5SubclassIterator(); // returns iterator of InseeNaf2003Lv5Subclass

// for the previous norm (1993) :
$sections   = $endpoint->getNaf1993Lv1SectionIterator();  // returns iterator of InseeNaf1993Lv1Section
$divisions  = $endpoint->getNaf1993Lv2DivisionIterator(); // returns iterator of InseeNaf1993Lv2Division
$groups     = $endpoint->getNaf1993Lv3GroupIterator();    // returns iterator of InseeNaf1993Lv3Group
$classes    = $endpoint->getNaf1993Lv4ClassIterator();    // returns iterator of InseeNaf1993Lv4Class
$subclasses = $endpoint->getNaf1993Lv5SubclassIterator(); // returns iterator of InseeNaf1993Lv5Subclass

// for the previous norm (1973) :
$sections   = $endpoint->getNap1973Lv1SectionIterator();  // returns iterator of InseeNap1973Lv1Section
$divisions  = $endpoint->getNap1973Lv2DivisionIterator(); // returns iterator of InseeNap1973Lv2Division
$groups     = $endpoint->getNap1973Lv3GroupIterator();    // returns iterator of InseeNap1973Lv3Group
$classes    = $endpoint->getNap1973Lv4ClassIterator();    // returns iterator of InseeNap1973Lv4Class

```

The `InseeNaf2003Lv5Subclass` also contains the default code for the 
`InseeNaf2008Lv5Subclass` records, which is made to ease the transitions from
the 2003 norm to the latest norm. Those transitions were written using the
transition tables on the insee, but in case of multiple transitions, the most
probable was hand chosen.

For This reason there may be errors in the transition table between the 2003
norm to the 2008 norm. Pull requests on the csv files are welcome if they try
to correct such transition choices.

The same is true for the relations between the 1993 norm with the 2003 norm (for
relations between `InseeNaf1993Lv5Subclass` to `InseeNaf2003Lv5Subclass`)
and is true for relations between the 1973 norm with the 1993 norm (for
relations between `InseeNap1973Lv4Class` to `InseeNaf1993Lv5Subclass`).


## License

- The code is under MIT (See [license file](LICENSE)).

- The data is under Open License ([English](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf) / [Français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))


- Source supplémentaires :
  - [Association A.C.T.E.](http://info.acte.free.fr/glossaire/detail.php?id=59)
